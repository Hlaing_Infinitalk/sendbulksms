const express = require('express');
const app = express();
const port = 3000;
const sendMessage = require('./sendBulkSms');
// Body parsing middleware
app.use(express.json());

// Send Bulk SMS endpoint
app.post('/api/sendBulkSMS', (req, res) => {
    const smsNumbers = req.body.numbers;
    const smsBody = req.body.body;

    if(smsNumbers && smsBody) {
        sendMessage(smsNumbers, smsBody)
        .then(result => {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({ message: 'Internal server error' });
        });
    } else {
        res.status(400).send('Your payload incorrect. Payload should numbers array and smsBody String Like {number: [], body: \'\'}')
    }
    
});

// Start the server
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
