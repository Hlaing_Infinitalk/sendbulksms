const sendMessage = (smsNumbers, smsBody) => {
  const accountSid = 'AC7473d96e1f3a16374f309fdf5e5b55b0';
  const authToken = '27b06ab64259ac97ab112b19d8356892';
  const client = require('twilio')(accountSid, authToken);
  const Bottleneck = require('bottleneck');
  const limiter = new Bottleneck({
    maxConcurrent: 100,
    minTime: 100
  });

  let successCount = 0;
  let errorCount = 0;
  let successNumbers = [];
  let errorNumbers = [];

  return new Promise((resolve, reject) => {
    smsNumbers.forEach((number) => {
      limiter.schedule(() =>
        client.messages.create({
            body: smsBody,
            from: '+13055639491',
            to: number
          })
          .then(message => {
            console.log(`Sent message to ${message.to}: ${message.sid}`);
            successCount++;
            successNumbers.push(message.to);

            client.messages(message.sid)
              .fetch()
              .then(message => console.log(`Message status for ${message.to}: ${message.status}`))
              .catch(error => console.log(`Error getting message status for ${number}: ${error}`));
          })
          .catch(error => {
            console.log(`Error sending message to ${number}: ${error}`);
            errorCount++;
            errorNumbers.push(number);
          })
      );
    });

    limiter.on('idle', () => {
      console.log(`Sent ${successCount} messages successfully and encountered ${errorCount} errors.`);
      resolve({
        successCount,
        errorCount,
        successNumbers,
        errorNumbers
      });
    });
  });
};

module.exports = sendMessage;