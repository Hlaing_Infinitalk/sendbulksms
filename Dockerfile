# Use an official Node.js runtime as a parent image
FROM node:14

# Set the working directory to /app
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application files to the container
COPY . .

# Set the port the container should listen on
ENV PORT 8080

# Expose the port
EXPOSE 8080

# Start the Node.js application
CMD [ "npm", "start" ]
